# Chess Plugin for Hubzilla - davide's version
<a href="" target="_blank"><img src="hubzilla-chess-1.png" width="80%"/></a>


[Hubzilla](http://hubzilla.org) is a powerful platform for creating interconnected websites featuring a decentralized identity, communications, and permissions framework built using common webserver technology. 

This is a plugin for Hubzilla (version 1.1.2 and higher) that allows you to play chess games with anyone on the grid. The chess board mechanics are powered by [chessboard.js](http://chessboardjs.com), rendering a smooth and intuitive interface. Some gameplay features include a move history viewer, as well as the ability to end and resume games without deleting them.

Information about moves is propagated between players using the standard Hubzilla post system, so notifications of new games and opponent moves appear in your normal stream. You can also post messages in the conversation in a natural way as you play a game.

To install, use the following commands (assuming `/var/www/` is your hub's web root):

```
cd /var/www/
util/add_addon_repo https://gitlab.com/zot/hubzilla-chess.git chess
util/update_addon_repo chess
```

Then enable the plugin through the admin settings interface.